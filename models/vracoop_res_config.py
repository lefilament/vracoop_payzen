# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, models, fields, _
from ..helpers import constants


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    payzen_site_id = fields.Char(
        string='Identifiant boutique', 
        related='website_id.payzen_site_id',
        readonly=False)
    payzen_key_test = fields.Char(
        string='Clé en mode test', 
        related='website_id.payzen_key_test',
        readonly=False)
    payzen_key_prod = fields.Char(
        string='Clé en mode production', 
        related='website_id.payzen_key_prod',
        readonly=False)

    @api.multi
    def execute_payzen(self):
        self.ensure_one()
        return self.execute